//
//  AppDelegate.h
//  ALMemoryLeak
//
//  Created by Tyler Tape on 11/22/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

